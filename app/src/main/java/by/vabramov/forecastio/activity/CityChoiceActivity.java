package by.vabramov.forecastio.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import by.vabramov.forecastio.R;
import by.vabramov.forecastio.util.ForecastPreferencesManager;
import by.vabramov.forecastio.util.Util;

/**
 * @author Vlad
 * @since 31.08.2015
 */
public class CityChoiceActivity extends AppCompatActivity {

    private ListView listView;

    private ForecastPreferencesManager preferencesManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_choice);

        preferencesManager = ForecastPreferencesManager.get();

        listView = (ListView) findViewById(R.id.city_list);

        String[] values = new String[]{
                getString(R.string.brest_city),
                getString(R.string.vitebsk_city),
                getString(R.string.gomel_city),
                getString(R.string.grodno_city),
                getString(R.string.minsk_city),
                getString(R.string.mogilev_city)};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                R.layout.list_item, android.R.id.text1, values);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String coordinates = "";
                switch (position) {
                    case 0:
                        coordinates = getString(R.string.brest);
                        break;
                    case 1:
                        coordinates = getString(R.string.vitebsk);
                        break;
                    case 2:
                        coordinates = getString(R.string.gomel);
                        break;
                    case 3:
                        coordinates = getString(R.string.grodno);
                        break;
                    case 4:
                        coordinates = getString(R.string.minsk);
                        break;
                    case 5:
                        coordinates = getString(R.string.mogilev);
                        break;
                }

                String cityName = (String) listView.getItemAtPosition(position);
                preferencesManager.persistString(Util.Constants.CITY_NAME_KEY, cityName);
                preferencesManager.persistString(Util.Constants.CITY_COORDS_KEY, coordinates);

                Intent intent = new Intent(CityChoiceActivity.this, HomeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (getIntent().getBooleanExtra(HomeActivity.EXTRA_ALLOW_GO_BACK, true)) {
            super.onBackPressed();
        }
    }
}
