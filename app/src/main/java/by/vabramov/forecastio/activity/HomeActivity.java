package by.vabramov.forecastio.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import by.vabramov.forecastio.R;
import by.vabramov.forecastio.model.DailyData;
import by.vabramov.forecastio.model.FullForecast;
import by.vabramov.forecastio.util.ForecastApi;
import by.vabramov.forecastio.util.ForecastPreferencesManager;
import by.vabramov.forecastio.util.Util;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class HomeActivity extends AppCompatActivity {

    public static final String EXTRA_ALLOW_GO_BACK = "allow_go_back";
    private static final String FILTER_ACTION = "android.net.conn.CONNECTIVITY_CHANGE";

    private AlertDialog updateDialog;

    private ImageView weatherIcon;
    private TextView curTempTextView;
    private TextView curMaxTempTextView;
    private TextView curMinTempTextView;
    private TextView summaryTextView;
    private TextView realFeelTextView;
    private TextView pressureTextView;
    private TextView sunriseTextView;
    private TextView sunsetTextView;
    private TextView lastUpdateTextView;
    private List<ImageView> dailyWeatherIcons = new ArrayList<>();
    private List<TextView> daysDescriptionsTextViews = new ArrayList<>();
    private List<TextView> daysDatesTextViews = new ArrayList<>();
    private List<TextView> maxTemperatureTextViews = new ArrayList<>();
    private List<TextView> minTemperatureTextViews = new ArrayList<>();
    private List<TextView> humidityTextViews = new ArrayList<>();

    private String[] daysOfWeek;
    private String cityCoordinates;
    private boolean isPreviouslyRestored = false;
    private boolean isFirstRequest = false;

    private ForecastPreferencesManager preferencesManager;
    private ConnectionChangeReceiver connectionChangeReceiver;

    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.btn_refresh:
                    if (Util.haveInternetConnection(HomeActivity.this)) {
                        requestWeather();
                    } else
                        Toast.makeText(HomeActivity.this, R.string.network_unavailable, Toast.LENGTH_LONG).show();
                    break;
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_change_city:
                startActivity(new Intent(this, CityChoiceActivity.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        preferencesManager = ForecastPreferencesManager.get();
        connectionChangeReceiver = new ConnectionChangeReceiver();

        init();

        cityCoordinates = preferencesManager.getStringValue(Util.Constants.CITY_COORDS_KEY, "");
        if (TextUtils.isEmpty(cityCoordinates)) {
            startActivity(new Intent(this, CityChoiceActivity.class)
                    .putExtra(EXTRA_ALLOW_GO_BACK, false));
            return;
        }
        restorePreviousForecast();
        requestWeather();
    }

    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(connectionChangeReceiver, new IntentFilter(FILTER_ACTION));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(connectionChangeReceiver);
    }

    private void requestWeather() {
        if (Util.haveInternetConnection(this)) {
            showUpdateDialog();
            isFirstRequest = true;
            Map<String, String> params = new HashMap<>();
            params.put(Util.Constants.FORECAST_UNIT_KEY, Util.Constants.FORECAST_UNIT_VALUE);
            params.put(Util.Constants.FORECAST_LANG_KEY, Util.getForecastLanguage());
            params.put(Util.Constants.FORECAST_EXCLUDE_KEY, "hourly");
            ForecastApi.getForecastService().getForecast(cityCoordinates, params,
                    new Callback<FullForecast>() {
                        @Override
                        public void success(FullForecast fullForecast, Response response) {
                            long curTime = Calendar.getInstance().getTime().getTime();
                            preferencesManager.persistLong(Util.Constants.FORECAST_LAST_UPDATE, curTime);
                            preferencesManager.persistString(Util.Constants.FORECAST_JSON_KEY,
                                    Util.getGson().toJson(fullForecast));
                            fillScreen(fullForecast);
                            hideUpdateDialog();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            hideUpdateDialog();
                        }
                    });
        } else {
            Toast.makeText(this, R.string.network_unavailable, Toast.LENGTH_LONG).show();
        }
    }

    private void restorePreviousForecast() {
        String forecastJSON = preferencesManager.getStringValue(Util.Constants.FORECAST_JSON_KEY, "");
        if (!TextUtils.isEmpty(forecastJSON) && !isPreviouslyRestored) {
            FullForecast fullForecast = Util.getGson().fromJson(forecastJSON, FullForecast.class);
            fillScreen(fullForecast);
            isPreviouslyRestored = true;
        }
    }

    private void fillScreen(FullForecast fullForecast) {
        long lastUpdateTime = preferencesManager.getLongValue(Util.Constants.FORECAST_LAST_UPDATE, 0);
        lastUpdateTextView.setText(getString(R.string.update, lastUpdateTime != 0 ?
                Util.getFormattedDateTime(lastUpdateTime) : "--"));

        weatherIcon.setImageDrawable(getResources().getDrawable(
                fullForecast.getCurrently().getIcon().getDrawableId()));
        curTempTextView.setText(getString(R.string.degrees_c,
                fullForecast.getCurrently().getTemperature()));
        summaryTextView.setText(fullForecast.getCurrently().getSummary());

        List<DailyData> dailyDataList = fullForecast.getDaily().getData();

        DailyData currentDayData = dailyDataList.get(0);
        curMaxTempTextView.setText(getString(R.string.degrees, currentDayData.getTemperatureMax()));
        curMinTempTextView.setText(getString(R.string.degrees, currentDayData.getTemperatureMin()));
        realFeelTextView.setText(getString(R.string.real_feel, fullForecast.getCurrently().getApparentTemperature()));
        pressureTextView.setText(getString(R.string.pressure,
                Util.mmBarsToMmHg(fullForecast.getCurrently().getPressure())));
        sunriseTextView.setText(getString(R.string.sunrise,
                Util.getFormattedTime(fullForecast.getTimezone(), currentDayData.getSunriseTime())));
        sunsetTextView.setText(getString(R.string.sunset,
                Util.getFormattedTime(fullForecast.getTimezone(), currentDayData.getSunsetTime())));

        for (int i = 1; i < dailyDataList.size() - 1; ++i) {
            DailyData dailyData = dailyDataList.get(i);
            Date date = new Date(dailyData.getTime() * 1000);
            dailyWeatherIcons.get(i - 1).setImageDrawable(getResources().getDrawable(
                    dailyData.getIcon().getDrawableId()));
            daysDescriptionsTextViews.get(i - 1).setText(daysOfWeek[date.getDay()]);
            daysDatesTextViews.get(i - 1).setText(
                    Util.getFormattedDay(fullForecast.getTimezone(), dailyData.getTime()));
            maxTemperatureTextViews.get(i - 1).setText(getString(R.string.degrees, dailyData.getTemperatureMax()));
            minTemperatureTextViews.get(i - 1).setText(getString(R.string.degrees, dailyData.getTemperatureMin()));
            humidityTextViews.get(i - 1).setText(getString(R.string.humidity, dailyData.getHumidity()));
        }
    }


    private void showUpdateDialog() {
        if (updateDialog == null) {
            LayoutInflater li = LayoutInflater.from(this);
            View view = li.inflate(R.layout.dialog_update, null);

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setView(view);

            updateDialog = builder.create();
            updateDialog.setCancelable(false);
        }
        if (!updateDialog.isShowing())
            updateDialog.show();
    }

    private void hideUpdateDialog() {
        if (updateDialog != null)
            updateDialog.dismiss();
    }

    private void init() {
        weatherIcon = (ImageView) findViewById(R.id.weather_icon);

        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_1));
        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_2));
        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_3));
        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_4));
        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_5));
        dailyWeatherIcons.add((ImageView) findViewById(R.id.weather_icon_day_6));

        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_1_str));
        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_2_str));
        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_3_str));
        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_4_str));
        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_5_str));
        daysDescriptionsTextViews.add((TextView) findViewById(R.id.lbl_day_6_str));

        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_1));
        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_2));
        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_3));
        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_4));
        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_5));
        daysDatesTextViews.add((TextView) findViewById(R.id.lbl_day_6));

        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_1));
        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_2));
        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_3));
        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_4));
        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_5));
        maxTemperatureTextViews.add((TextView) findViewById(R.id.lbl_max_temp_6));

        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_1));
        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_2));
        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_3));
        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_4));
        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_5));
        minTemperatureTextViews.add((TextView) findViewById(R.id.lbl_min_temp_6));

        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_1));
        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_2));
        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_3));
        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_4));
        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_5));
        humidityTextViews.add((TextView) findViewById(R.id.lbl_humidity_6));

        TextView curDateTextView = (TextView) findViewById(R.id.lbl_cur_date);
        TextView curCityTextView = (TextView) findViewById(R.id.lbl_cur_city);
        curTempTextView = (TextView) findViewById(R.id.lbl_cur_temp);
        curMaxTempTextView = (TextView) findViewById(R.id.lbl_cur_max_temp);
        curMinTempTextView = (TextView) findViewById(R.id.lbl_cur_min_temp);
        summaryTextView = (TextView) findViewById(R.id.lbl_summary);
        realFeelTextView = (TextView) findViewById(R.id.lbl_real_feel);
        pressureTextView = (TextView) findViewById(R.id.lbl_pressure);
        sunriseTextView = (TextView) findViewById(R.id.lbl_sunrise);
        sunsetTextView = (TextView) findViewById(R.id.lbl_sunset);
        lastUpdateTextView = (TextView) findViewById(R.id.lbl_last_updated);

        findViewById(R.id.btn_refresh).setOnClickListener(onClickListener);

        daysOfWeek = getResources().getStringArray(R.array.days_of_week);
        String[] daysOfWeekFull = getResources().getStringArray(R.array.days_of_week_full);

        curDateTextView.setText(Util.getFormattedDate() + ", " +
                daysOfWeekFull[Calendar.getInstance().get(Calendar.DAY_OF_WEEK) - 1]);

        String city = preferencesManager.getStringValue(Util.Constants.CITY_NAME_KEY, "");
        curCityTextView.setText(TextUtils.isEmpty(city) ? "--" : city);
    }

    public class ConnectionChangeReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Util.haveInternetConnection(context) && !isFirstRequest) {
                requestWeather();
            }
        }
    }
}
