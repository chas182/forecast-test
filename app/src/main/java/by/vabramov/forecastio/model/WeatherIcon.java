package by.vabramov.forecastio.model;

import by.vabramov.forecastio.R;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public enum WeatherIcon {
    CLEAR_DAY(R.drawable.ic_clear_day),
    CLEAR_NIGHT(R.drawable.ic_clear_night),

    RAIN(R.drawable.ic_rain),
    SNOW(R.drawable.ic_snow),
    SLEET(R.drawable.ic_sleet),

    WIND(R.drawable.ic_wind),

    FOG(R.drawable.ic_fog),
    CLOUDY(R.drawable.ic_cloudy),

    PARTLY_CLOUDY_DAY(R.drawable.ic_partly_cloudy_day),
    PARTLY_CLOUDY_NIGHT(R.drawable.ic_partly_cloudy_night);

    private final int drawableId;

    WeatherIcon(int drawableId) {
        this.drawableId = drawableId;
    }

    public String getFormattedName() {
        return name().toLowerCase().replaceAll("_", "-");
    }

    public int getDrawableId() {
        return drawableId;
    }
}
