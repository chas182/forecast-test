package by.vabramov.forecastio.model;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public class FullForecast {

    private String timezone;

    private CurrentlyForecast currently;
    private DailyForecast daily;

    public String getTimezone() {
        return timezone;
    }

    public CurrentlyForecast getCurrently() {
        return currently;
    }

    public DailyForecast getDaily() {
        return daily;
    }
}
