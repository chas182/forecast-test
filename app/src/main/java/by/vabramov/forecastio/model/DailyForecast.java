package by.vabramov.forecastio.model;

import java.util.List;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public class DailyForecast {

    List<DailyData> data;

    public List<DailyData> getData() {
        return data;
    }
}
