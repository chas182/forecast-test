package by.vabramov.forecastio.model;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public class CurrentlyForecast {

    private long time;
    private String summary;
    private WeatherIcon icon;
    private float temperature;
    private float apparentTemperature;
    private float pressure;


    public String getSummary() {
        return summary;
    }

    public long getTime() {
        return time;
    }

    public WeatherIcon getIcon() {
        return icon;
    }

    public int getTemperature() {
        return Math.round(temperature);
    }

    public int getApparentTemperature() {
        return Math.round(apparentTemperature);
    }

    public float getPressure() {
        return pressure;
    }
}
