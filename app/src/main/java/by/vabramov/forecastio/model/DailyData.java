package by.vabramov.forecastio.model;

/**
 * @author Vlad
 * @since 28.08.2015
 */
public class DailyData {
    private long time;
    private String summary;
    private WeatherIcon icon;
    private float temperatureMin;
    private float temperatureMax;
    private float humidity;
    private long sunriseTime;
    private long sunsetTime;

    public long getTime() {
        return time;
    }

    public String getSummary() {
        return summary;
    }

    public WeatherIcon getIcon() {
        return icon;
    }

    public int getHumidity() {
        return Math.round(humidity * 100);
    }

    public int getTemperatureMin() {
        return Math.round(temperatureMin);
    }

    public int getTemperatureMax() {
        return Math.round(temperatureMax);
    }

    public long getSunriseTime() {
        return sunriseTime;
    }

    public long getSunsetTime() {
        return sunsetTime;
    }
}
