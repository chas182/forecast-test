package by.vabramov.forecastio.util;

import android.content.SharedPreferences;

/**
 * @author Vlad
 * @since 27.08.2015
 */
public class ForecastPreferencesManager {
    private static ForecastPreferencesManager instance;

    private SharedPreferences mPreferences;

    private ForecastPreferencesManager(SharedPreferences sp) {
        mPreferences = sp;
    }

    public static void initFrom(SharedPreferences sp) {
        instance = new ForecastPreferencesManager(sp);
    }

    public static ForecastPreferencesManager get() {
        if (instance == null)
            throw new IllegalStateException("ForecastPreferencesManager wasn't initialized.");
        return instance;
    }

    public final void persistString(String key, String value) {
        mPreferences.edit().putString(key, value).apply();
    }

    public final String getStringValue(String key, String defaultValue) {
        return mPreferences.getString(key, defaultValue);
    }

    public final void persistLong(String key, long value) {
        mPreferences.edit().putLong(key, value).apply();
    }

    public final long getLongValue(String key, long defaultValue) {
        return mPreferences.getLong(key, defaultValue);
    }

}
