package by.vabramov.forecastio.util;

import java.util.Map;

import by.vabramov.forecastio.model.FullForecast;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.QueryMap;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public class ForecastApi {

    private static ForecastService forecastServiceInstance;

    public static ForecastService getForecastService() {
        if (forecastServiceInstance == null) {
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.BASIC)
                    .setConverter(new GsonConverter(Util.getGson()))
                    .setEndpoint("https://api.forecast.io")
                    .build();

            forecastServiceInstance = restAdapter.create(ForecastService.class);
        }
        return forecastServiceInstance;
    }

    public interface ForecastService {

        @GET("/forecast/" + Util.Constants.FORECAST_API_KEY + "/{latLon}")
        void getForecast(@Path("latLon") String latLon, @QueryMap Map<String, String> params,
                         Callback<FullForecast> callback);
    }

}
