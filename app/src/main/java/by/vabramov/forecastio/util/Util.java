package by.vabramov.forecastio.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import by.vabramov.forecastio.model.WeatherIcon;

/**
 * @author Vladislav Abramov
 * @since 28.08.2015
 */
public final class Util {

    private static Gson gson;

    static {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(WeatherIcon.class, new WeatherIconDeserializer());
        gson = gsonBuilder.create();
    }

    public static Gson getGson() {
        return gson;
    }

    public static boolean haveInternetConnection(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    public static String getForecastLanguage() {
        String deviceLang = Locale.getDefault().getLanguage();
        return Arrays.binarySearch(Constants.FORECAST_LANGS, deviceLang) != -1?
                deviceLang : Constants.FORECAST_LANG_DEFAULT_VALUE;
    }

    public static String getFormattedDateTime(long time) {
        DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy HH:mm");
        Date date = new Date(time);
        return dateFormatter.format(date);
    }

    public static String getFormattedTime(String timeZone,long time) {
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
        Date date = new Date(time*1000);
        return formatter.format(date);
    }

    public static String getFormattedDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("d MMMM");
        formatter.setTimeZone(TimeZone.getDefault());
        Date datetime = Calendar.getInstance().getTime();
        return formatter.format(datetime);
    }

    public static String getFormattedDay(String timeZone, long time){
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM");
        formatter.setTimeZone(TimeZone.getTimeZone(timeZone));
        Date datetime = new Date(time * 1000);
        return formatter.format(datetime);
    }

    /**
     * Converts milli bars to mmHg
     */
    public static int mmBarsToMmHg(float mmBars) {
        return (int) Math.round(mmBars * 0.7500637554192);
    }

    public interface Constants {
        String FORECAST_API_KEY = "33bba43e0c6732e2c767da52feceb15e";
        String FORECAST_EXCLUDE_KEY = "exclude";
        String FORECAST_UNIT_KEY = "units";
        String FORECAST_UNIT_VALUE = "si";
        String FORECAST_LANG_KEY = "lang";
        String FORECAST_LANG_DEFAULT_VALUE = "lang";
        String[] FORECAST_LANGS = {"ar", "bs", "de", "en", "es", "fr", "it", "nl", "pl", "pt", "ru",
                "sk", "sv", "tet", "tr", "uk", "x-pig-latin", "zh"};

        String FORECAST_LAST_UPDATE = "forecast_last_update";
        String FORECAST_JSON_KEY = "forecast_json";
        String CITY_NAME_KEY = "city_name";
        String CITY_COORDS_KEY = "city_coords";
    }

    private static class WeatherIconDeserializer implements JsonDeserializer<WeatherIcon> {
        @Override
        public WeatherIcon deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
                throws JsonParseException {
            WeatherIcon[] icons = WeatherIcon.values();
            for (WeatherIcon icon : icons) {
                if (icon.getFormattedName().equals(json.getAsString()) ||
                        icon.name().equals(json.getAsString()))
                    return icon;
            }
            return null;
        }
    }
}
