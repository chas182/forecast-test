package by.vabramov.forecastio;

import android.app.Application;
import android.preference.PreferenceManager;

import by.vabramov.forecastio.util.ForecastPreferencesManager;

/**
 * @author Vlad
 * @since 28.08.2015
 */
public class ForecastApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        ForecastPreferencesManager.initFrom(PreferenceManager.getDefaultSharedPreferences(this));

    }

}
